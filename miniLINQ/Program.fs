﻿module miniLINQ.Program

open System.IO
open miniLINQ.Ex_1
open miniLINQ.Ex_2
open miniLINQ.Ex_3

[<EntryPoint>]
let main argv =
    //Exercise 1
    let filterResult =
        [1..3] |> filter (isGreaterThen 2)  
    printfn "%A" filterResult

    let filter'Result = filter' (isGreaterThenOption 2) [1..3]
    printfn "%A" filter'Result
    
    //Exercise 2
    let maxResult = tryMax [1..100]
    if maxResult.IsSome then
        printfn "%A" maxResult.Value
    else
        printfn "%A" maxResult
    
    //Exercise 3
    let searchWords = ["beautiful"; "I'm"; "Arch"; "btw"; "universe"]
    
    let files = (Directory.EnumerateFiles @"..\..\..\textData\"
                 |> Seq.toList)
    
    let results =
        files
        |> (fun (files) -> simpleGrep files searchWords)
    
    //let results =
    //    simpleGrep
    //        searchWords
    //        (Directory.EnumerateFiles @"..\..\..\textData\"
    //         |> Seq.toList)
    //
    results |> List.iter (fun l -> printfn $"%A{l}")

    0