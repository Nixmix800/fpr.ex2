﻿module miniLINQ.Ex_2

//function for filter    
let isGreaterThen x y =
    y > x

let rec aggregate f s xs =
    match xs with
    | [] -> s
    | x::xs -> f x (aggregate f s xs)

//because we can (x > y) would also be fine I guess.
let greaterThen y x =
    if isGreaterThen y x then x
    else y

//Define a function which computes the maximum of the list
let tryMax (xs : list<int>) : Option<int> =
    match xs with
    | [] -> None
    | head::tail ->
        Some (aggregate greaterThen head tail)