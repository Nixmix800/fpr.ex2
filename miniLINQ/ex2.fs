﻿open System.IO

//Exercise 1:

//function for filter    
let isGreaterThen y x =
    x > y
    
let isGreaterThenOption y x =
    if x > y then
        Some x
    else
        None
let rec choose (f : 'a -> Option<'a>) (xs : list<'a>) : list<'a> =
    match xs with //go thru list
    | [] -> []
    | head::tail -> //if list element
        match f head with //call filter-function with current list-element
        | None -> //if filter-function returns None, skip element
            choose f tail
        | Some element -> //if filter-function returns Some, add element to list
            element :: choose f tail
     
// Define a function which filters a list of elements given a predicate.
let rec filter (f : 'a -> bool) (xs : list<'a>) : list<'a> =
    match xs with //go thru list
    | [] -> []
    | head::tail -> //if list element
        match f head with //call filter-function with current list-element
        | false -> //if filter-function returns false, skip element
            filter f tail
        | true  -> //if filter-function returns true, add element to list
            head :: filter f tail

//Bonus:
let filter' (f : 'a -> Option<'a>) i = i |> choose f

let filterResult =
    [1..3] |> filter (isGreaterThen 2)  
printfn "%A" filterResult

let filter'Result = filter' (isGreaterThenOption 2) [1..3]
printfn "%A" filter'Result

//Exercise 2:

let rec aggregate f s xs =
    match xs with
    | [] -> s
    | x::xs -> f x (aggregate f s xs)

//because we can (x > y) would also be fine I guess.
let greaterThen y x =
    if isGreaterThen y x then x
    else y

//Define a function which computes the maximum of the list
let tryMax (xs : list<int>) : Option<int> =
    match xs with
    | [] -> None
    | head::tail ->
        Some (aggregate greaterThen head tail)

let maxResult = tryMax [1..100]
if maxResult.IsSome then
    printfn "%A" maxResult.Value
else
    printfn "%A" maxResult

// Exercise 3:

// Copied from lecture
let rec append (xs : list<'a>) (ys : list<'a>) : list<'a> =
    match xs with
    | [] -> ys
    | head::tail -> 
        head :: append tail ys

let concat (xs : list<list<'a>>) : list<'a> =
    aggregate append [] xs

let rec map (f : 'a -> 'b) (xs : list<'a>) : list<'b> =
    match xs with
    | [] -> []
    | head::tail -> 
        f head :: map f tail

let collect (f : 'a -> list<'b>) (xs : list<'a>) : list<'b> =
    xs |> map f |> concat

let mapi (f : int -> 'a -> 'b) (xs : list<'a>) =
    let rec doIt i xs =
        match xs with
        | [] -> []
        | x::xs -> f i x :: doIt (i+1) xs
    doIt 0 xs

//Bonus:
type grepResults = {
    filename : string
    result : string
    lineNumber : int
    matchedWords : list<string>
}

let searchWordInLine (line : string) (searchWord : string) =
    line.Contains(searchWord)

let searchWordsInLine (line : string) (searchWords : list<string>) =
    searchWords
    |> filter (fun (sw) -> (searchWordInLine line) sw)
       
let indexed (input : list<'a>) : (list<int * 'a>) =
    input
    |> mapi (fun index inp -> (index, inp))

let findSearchWordsInFile (file: string) (searchWords: list<string>) =
    File.ReadAllLines file //read all lines in file
    |> Seq.toList
    |> indexed
    |> map (fun(index, line) -> index, line, (searchWordsInLine line searchWords))
    |> filter (fun (_, line, searchWords) -> searchWords.Length > 0)
    |> map (fun(index, line, searchWords) -> {
        filename=file; lineNumber=index+1; result=line; matchedWords=searchWords
    })
    
let simpleGrep (files: list<string>) (searchWords: list<string>) =
    files
    |> collect (fun(file) -> findSearchWordsInFile file searchWords)

let searchWords = ["beautiful"; "I'm"; "Arch"; "btw"; "universe"]

let files = (Directory.EnumerateFiles @".\textData\"
         |> Seq.toList)

let results =
    files
    |> (fun (files) -> simpleGrep files searchWords)

results |> List.iter (fun l -> printfn $"%A{l}")