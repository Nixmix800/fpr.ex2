﻿module miniLINQ.Ex_3

open System.IO
open miniLINQ.Ex_2
open miniLINQ.Ex_1

// find a function which concatenates two lists
let rec append (xs : list<'a>) (ys : list<'a>) : list<'a> =
    match xs with
    | [] -> ys
    | head::tail -> 
        head :: append tail ys

let concat (xs : list<list<'a>>) : list<'a> =
    aggregate append [] xs

let rec map (f : 'a -> 'b) (xs : list<'a>) : list<'b> =
    match xs with
    | [] -> []
    | head::tail -> 
        f head :: map f tail

// we have map, and concat at hand
// can we implement collect using those?
let collect (f : 'a -> list<'b>) (xs : list<'a>) : list<'b> =
    xs |> map f |> concat

let mapi (f : int -> 'a -> 'b) (xs : list<'a>) =
    let rec doIt i xs =
        match xs with
        | [] -> []
        | x::xs -> f i x :: doIt (i+1) xs
    doIt 0 xs

type grepResults = {
    filename : string
    result : string
    lineNumber : int
    matchedWords : list<string>
}

let searchWordInLine (line : string) (searchWord : string) =
    line.Contains(searchWord)

let searchWordsInLine (line : string) (searchWords : list<string>) =
    searchWords
    |> filter (fun (sw) -> (searchWordInLine line) sw)
       
let indexed (input : list<'a>) : (list<int * 'a>) =
    input
    |> mapi (fun index inp -> (index, inp))

let findSearchWordsInFile (file: string) (searchWords: list<string>) =
    File.ReadAllLines file //read all lines in file
    |> Seq.toList
    |> indexed
    |> map (fun(index, line) -> index, line, (searchWordsInLine line searchWords))
    |> filter (fun (_, line, searchWords) -> searchWords.Length > 0)
    |> map (fun(index, line, searchWords) -> {
        filename=file; lineNumber=index+1; result=line; matchedWords=searchWords
    })
    
let simpleGrep (files: list<string>) (searchWords: list<string>) =
    files
    |> collect (fun(file) -> findSearchWordsInFile file searchWords)