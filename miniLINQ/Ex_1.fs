module miniLINQ.Ex_1

//function for filter    
let isGreaterThen y x =
    x > y

let isGreaterThenOption y x =
    if x > y then
        Some x
    else
        None
let rec choose (f : 'a -> Option<'a>) (xs : list<'a>) : list<'a> =
    match xs with //go thru list
    | [] -> []
    | head::tail -> //if list element
        match f head with //call filter-function with current list-element
        | None -> //if filter-function returns None, skip element
            choose f tail
        | Some element -> //if filter-function returns Some, add element to list
            element :: choose f tail
     
// Define a function which filters a list of elements given a predicate.
let rec filter (f : 'a -> bool) (xs : list<'a>) : list<'a> =
    match xs with //go thru list
    | [] -> []
    | head::tail -> //if list element
        match f head with //call filter-function with current list-element
        | false -> //if filter-function returns false, skip element
            filter f tail
        | true  -> //if filter-function returns true, add element to list
            head :: filter f tail

//Bonus:
let filter' (f : 'a -> Option<'a>) i = i |> choose f