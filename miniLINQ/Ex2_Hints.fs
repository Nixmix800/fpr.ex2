﻿module miniLINQ.Ex2_Hints

open System.IO

let files' = 
    Directory.EnumerateFiles @"..\..\..\textData\"
    |> Seq.toList
    |> Seq.collect (fun filePath -> // aka. concatMap
        if Path.GetExtension filePath = ".txt" then 
            [File.ReadAllLines filePath]
        else []
    )

let files'' = 
    Directory.EnumerateFiles @"..\..\..\textData\"
    |> Seq.toList
    |> Seq.map (fun f -> if Path.GetExtension f = ".txt" then [File.ReadAllText f] else [])
    |> Seq.concat // aka flatMap 

let files''' = 
    Directory.EnumerateFiles @"..\..\..\textData\"
    |> Seq.toList
    |> Seq.map (fun f -> 
        if Path.GetExtension f = ".txt" then 
            [File.ReadAllText f |> Seq.map (fun line -> (f,line))]  // nested computations capture outer scope variables
        else 
            [])
    |> Seq.concat // aka flatMap 